from django.urls import path
from blog.views import ArticleListView, HomeView
urlpatterns = [
    path('', ArticleListView.as_view()),
    path('home/', HomeView.as_view())
]
