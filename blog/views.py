from django.apps import apps
from django.shortcuts import render

from django.views.generic.list import ListView
from django.views.generic.base import TemplateView

#def article_list(request):
#    articles = apps.get_model('blog.Article').objects.all()
#    context = {
#        'articles': articles
#    }
#    return render(request, 'blog/article_list.html', context)

class HomeView(TemplateView):
    template_name = 'blog/index2.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['moje'] = "MOJE"
        return context

class ArticleListView(ListView):
    model = apps.get_model('blog.Article')
    template_name = 'blog/article_lists.html'
    context_object_name = 'arts'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['moje'] = "MOJE"
        return context

    #def get_queryset(self):
    #    return apps.get_model('blog.Article').objects.filter(author='mateusz')