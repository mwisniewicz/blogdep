from django.apps import apps
from django.db import models

PUBLICATION_STATUSES = (
    ('u', 'Nieopublikowany'),
    ('p', 'Opublikowany')
)


data = [
    {
        'title': 'Pierwszy',
        'content': 'aaaaa',
        'author': 'Adam Nowak',
    },
    {
        'title': 'Drugi',
        'content': 'bbbb',
        'author': 'Jan Nowak',
    },
    {
        'title': 'Trzeci',
        'content': 'cccc',
        'author': 'Krzysztof Nowak',
    },
    {
        'title': 'Czwarty',
        'content': 'dddd',
        'author': 'Krzysztof Nowak',
    }
]

class AuthorManager(models.Manager):

    def register_from_entry(self, entry):
        obj, created = apps.get_model('blog.Author').objects.get_or_create(
                    first_name=entry['author'].split()[0],
                    last_name=entry['author'].split()[1])
        return obj


class Author(models.Model):
    first_name = models.CharField(max_length=20, verbose_name="Imię")
    last_name = models.CharField(max_length=20, verbose_name="Nazwisko")

    objects = AuthorManager()

    class Meta:
        verbose_name = "Autor"
        verbose_name_plural = "Autorzy"

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class ArticleManager(models.Manager):

    def register_from_data(self):
        data = [
        {
            'title': 'Pierwszy',
            'content': 'aaaaa',
            'author': 'Adam Nowak',
        },
        {
            'title': 'Drugi',
            'content': 'bbbb',
            'author': 'Jan Nowak',
        },
        {
            'title': 'Trzeci',
            'content': 'cccc',
            'author': 'Krzysztof Nowak',
        },
        {
            'title': 'Czwarty',
            'content': 'dddd',
            'author': 'Krzysztof Nowak',
        }]
        for entry in data:
            self.register_from_entry(entry)

    def register_from_entry(self, entry):
        return apps.get_model('blog.Article').objects.create(
                title=entry['title'],
                content=entry['content'],
                author=apps.get_model('blog.Author').objects.register_from_entry(entry)
            )

class Article(models.Model):
    title = models.CharField(max_length=128, verbose_name="Tytuł")
    content = models.TextField(verbose_name="Treść artykułu")
    author = models.ForeignKey("blog.Author", on_delete=models.SET_NULL, null=True, verbose_name="Autor")
    publication_datetime = models.DateTimeField(auto_now_add=True, verbose_name="Data publikacji")
    update_datetime = models.DateTimeField(auto_now=True, verbose_name="Data ostatniej modyfikacji")
    status = models.CharField(max_length=1, choices=PUBLICATION_STATUSES, default='u')

    objects = ArticleManager()

    class Meta:
        verbose_name = "Artykuł"
        verbose_name_plural = "Artykuły"

    def __str__(self):
        return f'{self.title} ({self.publication_datetime})'