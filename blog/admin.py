from django.contrib import admin

from blog.models import Article, Author

#admin.site.register(Article)
admin.site.register(Author)

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    fields = ('title', 'content', ('author', 'status'))
    list_display = ('title', 'author', 'status')
    ordering = ('-publication_datetime', )
    search_fields = ('title', )